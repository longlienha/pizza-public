using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ktltserver.Dto
{
    public class ProductDto
    {
        public string productIMG {get;set;}
        public string productName {get;set;}
        public string productShortdesc {get;set;}
        public string productPrice {get;set;}
        public string productSubtitle {get;set;}
        public string productCalories {get;set;}
        public string productMozarella {get;set;}
    }
}