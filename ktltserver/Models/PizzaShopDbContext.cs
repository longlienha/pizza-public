using Microsoft.EntityFrameworkCore;

namespace ktltserver.Models
{
    public class PizzaShopDbContext : DbContext
    {
        public PizzaShopDbContext(DbContextOptions<PizzaShopDbContext> options): base(options)
        {

        }
        public virtual DbSet<AllCodeModel> AllCode {get;set;}
        public virtual DbSet<BlogModel> Blog {get;set;}
        public virtual DbSet<CartModel> Cart {get;set;}
        public virtual DbSet<CategoryModel> Category {get;set;}
        public virtual DbSet<CustomerModel> Customer {get;set;}
        public virtual DbSet<MenuModel> Menu {get;set;}
        public virtual DbSet<OrderModel> Order {get;set;}
        public virtual DbSet<OrderProductModel> OrderProduct {get;set;}
        public virtual DbSet<ProductModel> Product {get;set;}
        public virtual DbSet<RestaurantModel> Restaurant {get;set;}
    }
}