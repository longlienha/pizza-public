using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ktltserver.Models
{
    public class CategoryModel
    {
        [Key]
        public int catId {get;set;}
        public string catImg {get;set;}
        public string catShortDesc {get;set;}
        public ICollection<ProductModel> product {get;set;}
    }
}