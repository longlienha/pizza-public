using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ktltserver.Models
{
    [Table("AllCode")]
    public class AllCodeModel
    {
        [Key]
        public int allCodeId {set; get;}
        [Required]
        public string type {set; get;}
        [Required]
        public string key {set; get;}
        [Required]
        public string value {set; get;}

    }
}