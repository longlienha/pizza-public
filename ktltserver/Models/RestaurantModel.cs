using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ktltserver.Models
{
    [Table("Restaurant")]
    public class RestaurantModel
    {
        [Key]
        public int resID {set; get;}
        [Required]
        public string resImg {set; get;}
        [Required]
        public string resTitle {set; get;}
        [Required]
        public string branceType {set; get;}
        [Required]
        public string location {set; get;}
        [Required]
        public string contactNo {set; get;}
        [Required]
        public string emailID {set; get;}
        [Required]
        public string mapData {set; get;}

    }
}