using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ktltserver.Models
{
    public class OrderModel
    {
        [Key]
        public int orderId {get;set;}
        public DateTime date {get;set;}
        public int price {get;set;}
        public int cusID {get;set;}
        public CustomerModel customer { get; set; }
        public ICollection<OrderProductModel> oderProduct {get;set;}
    }
}