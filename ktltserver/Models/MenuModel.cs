using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ktltserver.Models
{
    [Table("Menu")]
    public class MenuModel
    {
        [Key]
        public int menuId {set; get;}
        public string menuTitle {set; get;}
        [Required]
        public string price {set; get;}
        [Required]
        public string menuDesc {set; get;}

    }
}