using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ktltserver.Models
{
    [Table("Cart")]
    public class CartModel
    {
        [Key]
        public int cartId {set; get;}
        public string cartImg {set; get;}
        [Required]
        public string qty {set; get;}
        [Required]
        public string price {set; get;}
        [Required]
        public string cartName {set; get;}

    }
}