using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ktltserver.Models
{
    [Table("Customer")]
    public class CustomerModel
    {
        [Key]
        public int customerId {set; get;}
        [Required]
        public string customerName {set; get;}
        [Required]
        public string customerAddress {set; get;}
        [Required]
        public string phone {set; get;}
        [Required]
        public string gender {set; get;}
        [Required]
        public string dob {set; get;}
        public ICollection<OrderModel> orders {get;set;}
    }
}