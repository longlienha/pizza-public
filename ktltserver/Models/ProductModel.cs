using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ktltserver.Models
{
    public class ProductModel
    {
        [Key]
        public int productId {get;set;}
        public string productIMG {get;set;}
        public string productName {get;set;}
        public string productShortdesc {get;set;}
        public string productPrice {get;set;}
        public string productSubtitle {get;set;}
        public string productCalories {get;set;}
        public string productMozarella {get;set;}
        public bool active {get;set;}
        public ICollection<OrderProductModel> orderProduct {get;set;}
        public int? catId {get;set;}
        public CategoryModel cattegory {get;set;}
        public int? allCodeId {get;set;}
        public AllCodeModel allCode {get;set;}
        public int? menuId {get;set;}
        public MenuModel menu {get;set;}
    }
}