using System.ComponentModel.DataAnnotations;

namespace ktltserver.Models
{
    public class OrderProductModel
    {
        [Key]
        public int orderProductId {get;set;}
        public int orderId {get;set;}
        public OrderModel order {get;set;}
        public int productId {get;set;}
        public ProductModel product {get;set;}
        public int quantity {get;set;}
        public int price {get;set;}
    }
}