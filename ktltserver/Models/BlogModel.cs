using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ktltserver.Models
{
    [Table("Blog")]
    public class BlogModel
    {
        [Key]
        public int blogId {set; get;}
        [Required]
        public string blogImg {set; get;}
        [Required]
        public string blogTitle {set; get;}
        [Required]
        public string blogShortdesc {set; get;}
        [Required]
        public string postDate {set; get;}
        [Required]
        public string authorName {set; get;}
        [Required]
        public string authorImg {set; get;}
        [Required]
        public string aboutAuthor {set; get;}
        [Required]
        public string longDesc {set; get;}

    }
}