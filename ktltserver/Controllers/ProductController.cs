using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ktltserver.Authentication;
using ktltserver.Dto;
using ktltserver.Models;
using Microsoft.AspNetCore.Authorization;

namespace ktltserver.Controllers
{
    [Route("api/")]
    [ApiController]
    public class ProductController: ControllerBase
    {
        private readonly PizzaShopDbContext _context;
        public ProductController(PizzaShopDbContext context)
        {
            _context = context;
        }
        
        [Authorize]
        [HttpPost]
        [Route("create-new-product")]
        public IActionResult createProduct(ProductDto proPara)
        {
            var product = new ProductModel();
            product.productIMG = proPara.productIMG;
            product.productName = proPara.productName;
            product.productShortdesc = proPara.productShortdesc;
            product.productPrice = proPara.productPrice;
            product.productSubtitle = proPara.productSubtitle;
            product.productCalories = proPara.productCalories;
            product.productMozarella = proPara.productMozarella;
            product.active = false;
            try
            {
                _context.Product.Add(product);
                _context.SaveChanges();
                return Ok(new{
                    message = "Success"
                });
            }catch
            {
                return StatusCode(500);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("get-all-product")]
        public IActionResult getAllProduct()
        {
            var listPro = _context.Product.Where(record => record.active == true);
            if (listPro != null)
            {
                return Ok(new{
                    data= listPro
                });
            }
            return StatusCode(StatusCodes.Status500InternalServerError, new Response{Status = "Error", Message = "Don't have any product"});
        }

        [Authorize]
        [HttpGet]
        [Route("get-all-product/admin")]
        public IActionResult getAllProductAdmin()
        {
            var listPro = _context.Product;
            if (listPro != null)
            {
                return Ok(new{
                    data= listPro
                });
            }
            return StatusCode(StatusCodes.Status500InternalServerError, new Response{Status = "Error", Message = "Don't have any product"});
        }

        [Authorize]
        [HttpGet]
        [Route("active-product")]
        public IActionResult activeProduct(int id)
        {
            ProductModel product = _context.Product.FirstOrDefault(x => x.productId == id);
            if (product == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response{Status = "Error", Message = "Don't have any product"});
            }else if(product.active == true)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response{Status = "Error", Message = "Product has been activated"});
            }
            product.active = true;
            _context.SaveChanges();
            return Ok(new{
                message = "success"
            });
        }

        [Authorize]
        [HttpPut]
        [Route("edit-product/{id}")]
        public IActionResult editProduct(int id, ProductDto productDto)
        {
            var product = _context.Product.FirstOrDefault(x => x.productId == id);
            if (product != null)
            {
                product.productIMG = productDto.productIMG;
                product.productName = productDto.productName;
                product.productShortdesc = productDto.productShortdesc;
                product.productPrice = productDto.productPrice;
                product.productSubtitle = productDto.productSubtitle;
                product.productCalories = productDto.productCalories;
                product.productMozarella = productDto.productMozarella;
                try
                {
                    _context.SaveChanges();
                    return Ok(new{
                        Message = "Success"
                    });
                }
                catch
                {
                    StatusCode(StatusCodes.Status500InternalServerError, new Response{Status = "Error", Message = "Can't update data"});
                }
            }
            return StatusCode(StatusCodes.Status500InternalServerError, new Response{Status = "Error", Message = "Don't have any product"});
        }
    }
}